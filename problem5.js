import everyYear from './problem4.js'
const olderCars = (inventory) =>{

    if(!Array.isArray(inventory))
    {
        throw new Error("Invalid argument");
    }

    let data = everyYear(inventory);
    let oldCars = [];
    for(let i=0;i<data.length;i++)
    {
        if(data[i]<2000)
        {
            oldCars.push(data[i]);
        }
    }
    return oldCars;
}
export default olderCars;
