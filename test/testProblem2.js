import lastCar from '../problem2.js'
import inventoryData from '../data.js';

let lstCar = [];
try{
    lstCar = lastCar(inventoryData);
} catch(e){
    console.log(e);
}
console.log(`Last car is a ${lstCar.car_make} ${lstCar.car_model}`);

