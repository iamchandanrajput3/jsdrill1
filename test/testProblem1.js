import returnData from '../problem1.js';
import inventoryData from '../data.js';

let data =[];
try{
     data = returnData(inventoryData);
}
catch(e){
    console.log(e);
}

console.log(`Car 33 is a ${data.car_year} ${data.car_make} ${data.car_model}`);