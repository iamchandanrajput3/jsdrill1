import everyYear from '../problem4.js'
import inventoryData from '../data.js';

let carYears = [];

try{
    carYears = everyYear(inventoryData);
} catch(e){
    console.log(e);
}

console.log(carYears);