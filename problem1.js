export default function returnData(inventory){

    if(!Array.isArray(inventory))
    {
        throw new Error("Invalid Argument passed");
    }

    for(let i=0;i<inventory.length;i++)
    {
        if(inventory[i].id===33){
            return {car_make: inventory[i].car_make,car_model: inventory[i].car_model,car_year: inventory[i].car_year};
        }
    }
}
