export default function carModels(inventory){

    if(!Array.isArray(inventory))
    {
        throw new Error("Invalid argument");
    }

    let data = [];
    for(let i=0;i<inventory.length;i++)
    {
        data.push(inventory[i].car_model);
    }
    return data.sort(function(carA, carB){
        carA.toLowerCase() < carB.toLowerCase() ? -1 : 1;
    });
}
