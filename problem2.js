
export default function lastCar(inventory){

    if(!Array.isArray(inventory))
    {
        throw new Error("Invalid Argument passed");
    }

    let n = inventory.length;
    return {car_make: inventory[n-1].car_make,car_model: inventory[n-1].car_model};
 }

