export default function everyYear(inventory){

    if(!Array.isArray(inventory))
    {
        throw new Error("Invalid argument");
    }

    let carYears = [];
    
    for(let i=0;i<inventory.length;i++)
    {
        carYears.push(inventory[i].car_year);
    }
    return carYears;
}
