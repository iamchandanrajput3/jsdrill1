const onlyCars = (inventory) =>{

    if(!Array.isArray(inventory))
    {
        throw new Error("Invalid argument");
    }

    let onlyAudiBMW = [];
    for(let i=0;i<inventory.length;i++)
    {
        let car = inventory[i].car_make;
        if(car === 'BMW' || car === 'Audi')
        {
            onlyAudiBMW.push({id: inventory[i].id, car_make: inventory[i].car_make,car_model: inventory[i].car_model,car_year: inventory[i].car_year});
        }
    }
    return onlyAudiBMW;
}

export default onlyCars;